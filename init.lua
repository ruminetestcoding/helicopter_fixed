local speed_limit = 5
local heli_hp = 30

if minetest.setting_get('helicopter_fixed_speed') then
	speed_limit = tonumber(minetest.setting_get('helicopter_fixed_speed'))
end
if minetest.setting_get('helicopter_fixed_hp') then
	heli_hp = tonumber(minetest.setting_get('helicopter_fixed_hp'))
end

--
-- Helper functions
--
local function get_sign(i)
	if i == 0 then
		return 0
	else
		return i/math.abs(i)
	end
end

--
-- Heli entity
--

local heli = {
	physical = true,
	collisionbox = {-1, 0, -1, 1, 2.5, 1},
	makes_footstep_sound = false,
	collide_with_objects = true,
	visual = "mesh",
	mesh = "heli.x",
	textures = {"blades.png","blades.png","heli.png","Glass.png"},
	--Players
	driver = nil,
	passenger = nil,
	--Rotation
	yaw=0,
	--Speeds
	vx=0,
	vy=0,
	vz=0,
	soundHandle=nil
}

function heli.stop_heli(self, player)
	if self.driver and self.driver == player then
		self.driver:set_detach()
		self.driver = nil
	end
	if self.passenger and self.passenger == player then
		self.passenger:set_detach()
		self.passenger = nil
	end
	if not self.driver and not self.passenger then
		self.object:set_animation({x=0,y=1},0, 0)
		self.object:setvelocity({x=0, y=0, z=0})
		self.object:setacceleration({x=0, y=-10, z=0})
		minetest.sound_stop(self.soundHandle)
	end
end

function heli:on_rightclick(clicker)
	if not clicker or not clicker:is_player() then
		return
	end
	if self.driver and clicker == self.driver then
		self.stop_heli(self, clicker)
	elseif not self.driver then
		self.driver = clicker
		clicker:setpos(self.object:getpos())
		clicker:set_attach(self.object, "", {x=0,y=14,z=5}, {x=0,y=0,z=0})
		if not self.passenger then
			self.soundHandle = minetest.sound_play(
				{name="helicopter_fixed_motor"},
				{object = self.object, gain = 2.0, max_hear_distance = 32, loop = true}
			)
			self.object:set_animation({x=0,y=11},30, 0)
			-- reset values
			self.object:setvelocity({x=0, y=0, z=0})
			self.object:setacceleration({x=0, y=0, z=0})
			self.yaw = 0
			self.vx = 0
			self.vy = 0
			self.vz = 0
		elseif self.passenger == self.driver then
			self.passenger = nil
		end
	elseif self.passenger and clicker == self.passenger then
		self.stop_heli(self, clicker)
	else
		clicker:setpos(self.object:getpos())
		clicker:set_attach(self.object, "", {x=0,y=14,z=-4}, {x=0,y=0,z=0})
		self.passenger = clicker
	end
end

function heli:on_activate(staticdata, dtime_s)
	self.object:set_armor_groups({cracky=80,choppy=80,fleshy=80})
	self.object:set_hp(heli_hp)
	self.prev_y=self.object:getpos()
end

function heli:on_punch(puncher, time_from_last_punch, tool_capabilities, direction)
	if self.object:get_hp() == 0 then
		if self.driver then
			self.driver:set_detach()
		end
		if self.soundHandle then
			minetest.sound_stop(self.soundHandle)
		end
		self.object:remove()
		
		if puncher and puncher:is_player() then
			puncher:get_inventory():add_item("main", {name="default:steel_ingot", count=5})
			puncher:get_inventory():add_item("main", {name="default:mese_crystal"})
		end
	end
end

function heli:on_step(dtime)
	if not self.driver then
		return
	end
	
	--Prevent multi heli control bug
	
	local opos = self.object:getpos()
	
	local function is_missing(player)
        if not player then
            return true
        end
		local pos = player:getpos()
		if not self.driver:is_player_connected() or (
			math.abs(pos.x - opos.x) > 3 or
			math.abs(pos.y - opos.y) > 3 or
			math.abs(pos.z - opos.z) > 3
		) then
			self.stop_heli(self, player)
			return true
		end
		return false
	end
	
	if is_missing(self.driver) and is_missing(self.passenger) then
		return
	end
	
	self.yaw = self.driver:get_look_yaw()
	self.vx = self.object:getvelocity().x
	self.vy = self.object:getvelocity().y
	self.vz = self.object:getvelocity().z
	self.object:setyaw(self.yaw-math.pi/2)
	local ctrl = self.driver:get_player_control()
	--Forward/backward
	if ctrl.up then
		self.vx = self.vx + math.cos(self.yaw)*0.1
		self.vz = self.vz + math.sin(self.yaw)*0.1
	end
	if ctrl.down then
		self.vx = self.vx-math.cos(self.yaw)*0.1
		self.vz = self.vz-math.sin(self.yaw)*0.1
	end
	--Left/right
	if ctrl.left then
		self.vz = self.vz+math.cos(self.yaw)*0.1
		self.vx = self.vx+math.sin(math.pi+self.yaw)*0.1
	end
	if ctrl.right then
		self.vz = self.vz-math.cos(self.yaw)*0.1
		self.vx = self.vx-math.sin(math.pi+self.yaw)*0.1
	end
	--up/down
	if ctrl.jump then
		if self.vy<1.5 then
			self.vy = self.vy+0.2
		end
	end
	if ctrl.sneak then
		if self.vy>-1.5 then
			self.vy = self.vy-0.2
		end
	end
	
	if self.vx==0 and self.vz==0 and self.vy==0 then
		return
	end
	--Decelerating
	local sx=get_sign(self.vx)
	self.vx = self.vx - 0.02*sx
	local sz=get_sign(self.vz)
	self.vz = self.vz - 0.02*sz
	local sy=get_sign(self.vy)
	self.vy = self.vy-0.01*sy
	
	--Stop
	if sx ~= get_sign(self.vx) then
		self.vx = 0
	end
	if sz ~= get_sign(self.vz) then
		self.vz = 0
	end
	if sy ~= get_sign(self.vy) then
		self.vy = 0
	end
	
	--Speed limit
	if math.abs(self.vx) > speed_limit then
		self.vx = speed_limit*get_sign(self.vx)
	end
	if math.abs(self.vz) > speed_limit then
		self.vz = speed_limit*get_sign(self.vz)
	end
	if math.abs(self.vy) > speed_limit then
		self.vz = speed_limit*get_sign(self.vy)
	end
	
	--Set speed to entity
	self.object:setvelocity({x=self.vx, y=self.vy,z=self.vz})
	self.object:setsprite(
		{
			x=-90+self.vx*3*math.cos(self.yaw)+self.vz*3*math.sin(self.yaw), 
			y=0-self.vx*3*math.sin(self.yaw)+self.vz*3*math.cos(self.yaw), 
		}
	)
end

--
--Registration
--

minetest.register_entity("helicopter_fixed:heli", heli)

--
--Craft items
--

--Blades
minetest.register_craftitem("helicopter_fixed:blades",{
	description = "Blades",
	inventory_image = "blades_inv.png",
	wield_image = "blades_inv.png",
})
--Cabin
minetest.register_craftitem("helicopter_fixed:cabin",{
	description = "Cabin for heli",
	inventory_image = "cabin_inv.png",
	wield_image = "cabin_inv.png",
})
--Heli
minetest.register_craftitem("helicopter_fixed:heli", {
	description = "Helicopter",
	inventory_image = "heli_inv.png",
	wield_image = "heli_inv.png",
	wield_scale = {x=1, y=1, z=1},
	liquids_pointable = false,
	
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type ~= "node" then
			return
		end
		if minetest.get_node(pointed_thing.above).name ~= "air" then
			return
		end
		minetest.add_entity(pointed_thing.above, "helicopter_fixed:heli")
		itemstack:take_item()
		return itemstack
	end,
})

--
--Craft
--

minetest.register_craft({
	output = 'helicopter_fixed:blades',
	recipe = {
		{'', 'default:steel_ingot', ''},
		{'default:steel_ingot', 'group:stick', 'default:steel_ingot'},
		{'', 'default:steel_ingot', ''},
	}
})
minetest.register_craft({
	output = 'helicopter_fixed:cabin',
	recipe = {
		{'', 'group:wood', ''},
		{'group:wood', 'default:mese_crystal','default:glass'},
		{'group:wood','group:wood','group:wood'},		
	}
})		
minetest.register_craft({
	output = 'helicopter_fixed:heli',
	recipe = {
		{'', 'helicopter_fixed:blades', ''},
		{'helicopter_fixed:blades', 'helicopter_fixed:cabin',''},	
	}
})	

