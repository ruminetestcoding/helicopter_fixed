# Minetest 0.4.7+ mod: Simple helicopter

**by Pavel_S**

## License of source code:

* GPL_v2

## License of media (textures and sounds):

* Attribution Noncommercial: helicopter_motor.ogg **by Robinhood76**

## Fork

**by InterVi**

Original mod:

* https://forum.minetest.net/viewtopic.php?id=6183
* https://github.com/SokolovPavel/helicopter

Fixed and improved.

## How to use

There are two seats: pilot and passenger. Attach and detach on the right click. The passenger can take the empty seat of the pilot right click. If the helicopter is empty, it falls. Move is carried out by keys WASD, SHIFT and SPACE. Gradual acceleration in a given direction. The helicopter takes damage from punches and can be broken (default hp: 30).
